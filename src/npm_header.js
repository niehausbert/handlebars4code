/* ---------------------------------------
 Exported Module Variable: Handlebars4Code
 Package:  handlebars4code
 Version:  1.2.38  Date: 2020/10/07 14:51:21
 Homepage: https://gitlab.com/niehausbert/handlebars4code#readme
 Author:   Engelbert Niehaus
 License:  MIT
 Date:     2020/10/07 14:51:21
 Require Module with:
    const Handlebars4Code = require('handlebars4code');
 JSHint: installation with 'npm install jshint -g'
 ------------------------------------------ */

/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */
