## NPM Library Information
* Exported Module Variable: `Handlebars4Code`
* Package:  `handlebars4code`
* Version:  `1.2.38`   (last build 2020/10/07 14:51:21)
* Homepage: `https://gitlab.com/niehausbert/handlebars4code#readme`
* License:  MIT
* Date:     2020/10/07 14:51:21
* Require Module with:
```javascript
    const vHandlebars4Code = require('handlebars4code');
```
* JSHint: installation can be performed with `npm install jshint -g`
